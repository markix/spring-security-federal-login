# 支持 表单登录(FormLogin)，又支持 三方帐号登录(OAuth2Login)
> 联邦认证
> 基于 spring-security、spring-security-oauth2-client 模块实现
> 基于 H2 数据库，可自行更换
> 初始用户见 src/main/resources/sql/data.sql，默认每次启动都会清空并插入！！！

## 大前提
三方用户需要先和本地用户绑定，三方登录时，实际上是找到对应的本地用户进行后台登录。所以，三方用户登录实际也是用户登录。（认证实体是本地用户）
三方用户和本地用户通过关联表记录绑定关系。

## 功能点
- 表单登录
- 三方帐号和本地用户绑定、解绑
- 三方帐号登录

## 流程
- 用户登录
- 用户登录后，主动点击绑定/解绑三方帐号
- 三方帐号登录
    - 已绑定用户 -》 找到本地用户，登录成功
    - 未绑定用户 -》 跳绑定页，用户输帐号密码，登录并绑定

### 绑定流程
- 用户登录后 -> 再绑定三方账号（走第三方登录流程）
FormLoginAuthenticationSuccessHandler -> TempAuthFilter -> OAuth2LoginAuthenticationSuccessHandler
- 三方账号登录后，再登录用户
OAuth2LoginAuthenticationSuccessHandler -> login-bind.html -> FormLoginAuthenticationSuccessHandler

## 核心组件
- login.html 自定义登录页面（表单登录、三方帐号登录）
- login-bind.html 自定义绑定页面（三方帐号登录时，发现没有绑定本地用户，进入此页面进行绑定操作）
- index.html 首页（可绑定、解绑三方帐号）

- TempAuthFilter
当用户登录后（会生成SecurityContext），再执行OAuth2Login流程（绑定）时又会生成三方用户的SecurityContext，（会覆盖原来的上下文）
为了避免原有的SecurityContext被覆盖，需要将用户认证上下文实体临时存储起来，用于后续还原认证上下文。

- OAuth2LoginAuthenticationSuccessHandler
自定义 OAuth2Login 的 AuthenticationSuccessHandler，要根据三方用户是否被绑定以及是否存在用户认证上下文做不同处理：**绑定**、**跳绑定页(登录后自动绑定)**、**登录**。

- OAuth2LoginAuthenticationFailureHandler
自定义 OAuth2Login 的 AuthenticationFailureHandler，三方授权失败的情况：要么绑定失败，要么三方登录失败。

- FormLoginAuthenticationSuccessHandler
自定义 FormLogin 的 AuthenticationSuccessHandler，处理登录后自动绑定逻辑。
