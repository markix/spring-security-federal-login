package com.markix.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


/**
 * @author markix
 */
@Data
@Accessors(chain = true)
@Entity
@Table(name = "user_third_auth")
public class UserToAuthPO {

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "idGenerator")
    @GenericGenerator(name = "idGenerator", strategy = "uuid2")
    private String id;

    @Column(name = "user_id")
    private String userId;

    /**
     * 三方平台类型
     */
    @Column(name = "type")
    private String type;

    /**
     * 三方用户唯一标识
     */
    @Column(name = "identity")
    private String identity;

    @Column(name = "bind_time")
    private String time;

}
