package com.markix.security.oauth2login;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author markix
 * @date 2022/9/5 23:56
 */
public class TempAuthFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        TempAuthContext.set(SecurityContextHolder.getContext().getAuthentication());
        filterChain.doFilter(request, response);
        TempAuthContext.clear();
    }

}
