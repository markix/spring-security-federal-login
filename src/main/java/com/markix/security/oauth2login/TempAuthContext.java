package com.markix.security.oauth2login;

import org.springframework.security.core.Authentication;

/**
 * 临时存放认证上下文
 *
 * <p/>用户登录后会生成 SecurityContext，此时通过 oauth2Login 也会产生SecurityContext（且会覆盖原有SC），
 * 所以需要在覆盖操作前将原先的SecurityContext暂存起来，处理完绑定后，再还原。
 *
 * @author markix
 * @date 2022/9/2 17:28
 */
class TempAuthContext {

    private static final ThreadLocal<Authentication> TL = ThreadLocal.withInitial(() -> null);

    public static Authentication get(){
        return TL.get();
    }

    public static void set(Authentication content){
        TL.set(content);
    }

    public static void clear(){
        TL.remove();
    }

    public static boolean notEmpty(){
        return get() != null;
    }

}
