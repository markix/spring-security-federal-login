package com.markix.security.oauth2login;

/**
 * 三方平台类型枚举
 *
 * @author markix
 * @date 2022/9/2 23:59
 */
@SuppressWarnings("all")
public enum ThirdPlatformType {

    Gitee("gitee.com"),
    GitHub("github.com"),
    Sina("sina.com"),
    WebXin("open.weixin.qq.com"),
    QQ("connect.qq.com")
    ;

    private final String domain;

    ThirdPlatformType(String domain) {
        this.domain = domain;
    }


    public static ThirdPlatformType parse(final String authorizationUri){
        String uri = authorizationUri;
        for (ThirdPlatformType type : ThirdPlatformType.values()) {
            if(uri.contains(type.domain)){
                return type;
            }
        }
        throw new RuntimeException("未支持的平台类型");
    }


    @Override
    public String toString() {
        return name();
    }

}
