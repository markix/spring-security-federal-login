/*
 * Copyright 2002-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.markix.security.oauth2login;


public final class WebAttributes {

    private WebAttributes() {
    }

    /**
     * 三方登录认证
     */
    public static final String THIRD_TYPE = "third_type";
    public static final String THIRD_AUTHENTICATION = "third_authentication";

    public static final String LOGIN_BIND = "login_bind";


    public static final String loginApi = "/login";
    public static final String loginPage = "/login.html";

    public static final String loginError = loginPage + "?error";
    public static final String loginSuccess = "/";
    public static final String oauth2LoginError = loginPage + "?oauth_error";
    public static final String oauth2LoginSuccess = "/?oauth2_login";

    public static final String bindPage = "/login-bind.html";
    public static final String bindSuccess = "/?bind_success";
    public static final String bindFailure = "/?bind_failure";

}
