package com.markix.dao;

import com.markix.entity.UserToAuthPO;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * @author markix
 */
@Repository
public interface UserToAuthDao extends JpaRepositoryImplementation<UserToAuthPO, String> {

    Optional<UserToAuthPO> findByTypeAndIdentity(String type, String identity);

    List<UserToAuthPO> findByUserId(String userId);

    boolean existsByTypeAndIdentity(String type, String identity);

    @Transactional
    void deleteByIdAndUserId(String id, String userId);

}
