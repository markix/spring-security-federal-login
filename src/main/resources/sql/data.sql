-- 每次都清空数据！
delete from sys_user;

-- BCryptPasswordEncoder 123456 -> $2a$10$shcts4vp58TzPiOrRfPiMu9LPjuC50KxdXM6In2TyzHnFT5pdIF7y
insert into sys_user(id, name, nickname, password) values
('usr-admin', 'admin', '管理员', '$2a$10$shcts4vp58TzPiOrRfPiMu9LPjuC50KxdXM6In2TyzHnFT5pdIF7y'),
('usr-zhangsan', 'zhangsan', '张三', '$2a$10$shcts4vp58TzPiOrRfPiMu9LPjuC50KxdXM6In2TyzHnFT5pdIF7y');

